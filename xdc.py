"""
This program takes a text file from the GA solver and converts it into a
XDC file to be used by Vivado while implementing a design. This is based
off an Artix-7 200T device.

To run the program you need to specify the location of the results.dat file

python3 xdc.py filename

"""
import sys
import re
import os

def print_help_info():
  print("\nUse of the program:")
  print("-------------------")
  print("To run the program use the following syntax.")
  print("python3 xdc.py opt opt ...")
  print("Types of options are shown below.")
  print("-h: Prints out this information.")
  print("-c filename: Specifies the location for the new xdc file.")
  print("-i filename: Specifies the results file to be decoded.")
  print("-r: Specifies if the research microblaze region is to be used.\n")

def extract_regions(fileName):
  f = open(fileName,"r")
  lines = []
  regions = []
  lines = f.readlines()
  for line in lines:
    match = re.findall("REGION.",line)
    if match:
      # Then this line is related to one of the regions
      line = line[8:]
      #extract the region name
      region_name = line[:line.index(" ")]
      line = line[line.index(" ")+1:]

      # Remove unneccessary information
      line = line[line.index(" ")+1:]
      line = line[line.index(" ")+1:]

      # Calculate the coordinates for teh region
      x1 = int(line[:line.index("-")])
      line = line[line.index("-")+1:]

      y1 = int(line[:line.index(" ")])
      line = line[line.index(" ")+1:]
      line = line[line.index(" ")+1:]

      x2 = int(line[:line.index("-")])
      line = line[line.index("-")+1:]

      y2 = int(line)

      # Create a tuple containing region info and add it to the region list
      t = (region_name,x1,x2,y1,y2)
      regions.append(t)
  # Close the file
  f.close()

  return regions

def extract_standard_constraints(xdc_file):
  f = open(xdc_file,"r")
  constraints = []
  lines = []
  lines = f.readlines()
  for line in lines:
    #Any line with pblock in it will relate to a pblock
    match1 = re.findall(".pblock.",line)
    if not match1 and line != "\n":
      #Then this line is not a pblock constraint and it should be included
      constraints.append(line)
  return constraints

def write_standard_constraints(xdc_file,constraints):
  for line in constraints:
    xdc_file.write(line)

def create_pblock(region_name,xdc_file):
  s = "create_pblock " + region_name + "\n"
  xdc_file.write(s)

  s = "add_cells_to_pblock [get_pblocks " + region_name + "] [get_cells -quiet [list " + region_name + "]]\n"
  xdc_file.write(s)

def write_slice_constraints(region_name,x1,x2,y1,y2,xdc_file):
  first_clb_col = -1
  last_clb_col = -1
  i = x1
  if is_clb_col(x1) == 0 :
    # Need to find first clb col
    while first_clb_col == -1 and i < x2 :
      i = i + 1
      if is_clb_col(i) == 1:
      	# Then found the first clb column
      	first_clb_col = i
  else :
    first_clb_col = x1

  if first_clb_col != -1 :
    # Then there will be a constraint to add
    # Need to find the final col
    i = x2
    if is_clb_col(i) == 0 :
    # Need to find last clb col
      while last_clb_col == -1 and i >= x1 :
        i = i - 1
        if is_clb_col(i) == 1:
      	  # Then found the last clb column
      	  last_clb_col = i
    else:
      #then x2 is the last_clb_col
      last_clb_col = x2

    # Now need to determine the slice column #s, it is not based off the col num
    start_x = 2*get_slice_col(first_clb_col)
    end_x = 2*get_slice_col(last_clb_col)+1 #each clb has 2 slice columns
    start_y = 50*y1
    end_y = 50*(y2+1)-1

    #Slight modification for the microblaze region
    if region_name == "fip_inst":
      start_x = start_x
      start_y = start_y +5
      end_x = end_x -2
      end_y = end_y -5

    s = "resize_pblock [get_pblocks " + region_name + "] -add {SLICE_X"+str(start_x)+"Y"+str(start_y)+":SLICE_X"+str(end_x)+"Y"+str(end_y)+"}\n"
    xdc_file.write(s)

def write_dsp_constraints(region_name,x1,x2,y1,y2,xdc_file):
  first_dsp_col = -1
  last_dsp_col = -1
  i = x1
  if is_dsp_col(x1) == 0 :
    # Need to find first dsp col
    while first_dsp_col == -1 and i < x2 :
      i = i + 1
      if is_dsp_col(i) == 1:
      	# Then found the first dsp column
      	first_dsp_col = i
  else :
    first_dsp_col = x1

  if first_dsp_col != -1 :
    # Then there will be a constraint to add
    # Need to find the final col
    i = x2
    if is_dsp_col(i) == 0 :
    # Need to find last dsp col
      while last_dsp_col == -1 and i >= x1 :
        i = i - 1
        if is_dsp_col(i) == 1:
      	  # Then found the last dsp column
      	  last_dsp_col = i
    else:
      #then x2 is the last_dsp_col
      last_dsp_col = x2

    # Now need to determine the ds column #s, it is not based off the col num
    start_x = get_dsp_col(first_dsp_col)
    end_x = get_dsp_col(last_dsp_col)
    start_y = 20*y1
    end_y = 20*(y2+1) -1 #-1 so it doesnt overlap with the other region

    s = "resize_pblock [get_pblocks " + region_name + "] -add {DSP48_X"+str(start_x)+"Y"+str(start_y)+":DSP48_X"+str(end_x)+"Y"+str(end_y)+"}\n"
    xdc_file.write(s)

def write_bram_constraints(region_name,x1,x2,y1,y2,xdc_file):
  first_bram_col = -1
  last_bram_col = -1
  i = x1
  if is_bram_col(x1) == 0 :
    # Need to find first bram col
    while first_bram_col == -1 and i < x2 :
      i = i + 1
      if is_bram_col(i) == 1:
      	# Then found the first bram column
      	first_bram_col = i
  else :
    first_bram_col = x1

  if first_bram_col != -1 :
    # Then there will be a constraint to add
    # Need to find the final col
    i = x2
    if is_bram_col(i) == 0 :
    # Need to find last dsp col
      while last_bram_col == -1 and i >= x1 :
        i = i - 1
        if is_bram_col(i) == 1:
      	  # Then found the last dsp column
      	  last_bram_col = i
    else:
      #then x2 is the last_dsp_col
      last_bram_col = x2

    # Now need to determine the dsp column #s, it is not based off the col num
    start_x = get_bram_col(first_bram_col)
    end_x = get_bram_col(last_bram_col)
    start_y = 20*y1
    end_y = 20* (y2+1) -1 #-1 so it doesnt overlap with the other region

    s = "resize_pblock [get_pblocks " + region_name + "] -add {RAMB18_X"+str(start_x)+"Y"+str(start_y)+":RAMB18_X"+str(end_x)+"Y"+str(end_y)+ "}\n"
    xdc_file.write(s)

def write_other_pblock_constraints(region_name,xdc_file,research):
  if region_name == "fip_inst" and research:
    s = "set_property CONTAIN_ROUTING 1 [get_pblocks " + region_name + "]\n"
    xdc_file.write(s)
    s = "set_property EXCLUDE_PLACEMENT 1 [get_pblocks " + region_name + "]\n"
    xdc_file.write(s)

# Checks to see if the given column is a slice column
def is_clb_col(col):
  if (col==6 or col==17 or col==28 or col==40 or col==51 or col==58 or col==69 or col==88 or col==99 or
    col==9 or col==14 or col==31 or col==43 or col==48 or col==61 or col==66 or col==91 or col==96 or
    col==55 or col==24 or col==0 or col==105 or col==1 or col==104):

    # Then the column is not a clb column, therefore not a slice column
    return 0
  return 1

# Returns the column number for the slice as slice columns ignore other columns and just use slice cols
def get_slice_col(col):
  x = -1
  if col == 2 :
    x = 0
  if col == 3 :
    x = 1
  if col == 4 :
    x = 2
  if col == 5 :
    x = 3
  if col == 7 :
    x = 4
  if col == 8 :
    x = 5
  if col == 10 :
    x = 6
  if col == 11 :
    x = 7
  if col == 12 :
    x = 8
  if col == 13 :
    x = 9
  if col == 15 :
    x = 10
  if col == 16 :
    x = 11
  if col == 18 :
    x = 12
  if col == 19 :
    x = 13
  if col == 20 :
    x = 14
  if col == 21 :
    x = 15
  if col == 22 :
    x = 16
  if col == 23 :
    x = 17
  if col == 25 :
    x = 18
  if col == 26 :
    x = 19
  if col == 27 :
    x = 20
  if col == 29 :
    x = 21
  if col == 30 :
    x = 22
  if col == 32 :
    x = 23
  if col == 33 :
    x = 24
  if col == 34 :
    x = 25
  if col == 35 :
    x = 26
  if col == 36 :
    x = 27
  if col == 37 :
    x = 28
  if col == 38 :
    x = 29
  if col == 39 :
    x = 30
  if col == 41 :
    x = 31
  if col == 42 :
    x = 32
  if col == 44 :
    x = 33
  if col == 45 :
    x = 34
  if col == 46 :
    x = 35
  if col == 47 :
    x = 36
  if col == 49 :
    x = 37
  if col == 50 :
    x = 38
  if col == 52 :
    x = 39
  if col == 53 :
    x = 40
  if col == 54 :
    x = 41
  if col == 56 :
    x = 42
  if col == 57 :
    x = 43
  if col == 59 :
    x = 44
  if col == 60 :
    x = 45
  if col == 62 :
    x = 46
  if col == 63 :
    x = 47
  if col == 64 :
    x = 48
  if col == 65 :
    x = 49
  if col == 67 :
    x = 50
  if col == 68 :
    x = 51
  if col == 70 :
    x = 52
  if col == 71 :
    x = 53
  if col == 72 :
    x = 54
  if col == 73 :
    x = 55
  if col == 74 :
    x = 56
  if col == 75 :
    x = 57
  if col == 76 :
    x = 58
  if col == 77 :
    x = 59
  if col == 78 :
    x = 60
  if col == 79 :
    x = 61
  if col == 80 :
    x = 62
  if col == 81 :
    x = 63
  if col == 82 :
    x = 64
  if col == 83 :
    x = 65
  if col == 84 :
    x = 66
  if col == 85 :
    x = 67
  if col == 86 :
    x = 68
  if col == 87 :
    x = 69
  if col == 89 :
    x = 70
  if col == 90 :
    x = 71
  if col == 92 :
    x = 72
  if col == 93 :
    x = 73
  if col == 94 :
    x = 74
  if col == 95 :
    x = 75
  if col == 97 :
    x = 76
  if col == 98 :
    x = 77
  if col == 100 :
    x = 78
  if col == 101 :
    x = 79
  if col == 102 :
    x = 80
  if col == 103 :
    x = 81
  return x

# Checks to see if the given column is a bram column
def is_bram_col(col):
  if col==6 or col==17 or col==28 or col==40 or col==51 or col==58 or col==69 or col==88 or col==99:
  	return 1
  return 0

# Returns the column number for the barm as bram columns dont use board cols for constraints
def get_bram_col(col):
  x = -1
  if col == 6 :
    x = 0
  if col == 17 :
    x = 1
  if col == 28 :
    x = 2
  if col == 40 :
    x = 3
  if col == 51 :
    x = 4
  if col == 58 :
    x = 5
  if col == 69 :
    x = 6
  if col == 88 :
    x = 7
  if col == 99 :
    x = 8

  return x

# Checks to see if the given column is a dsp column
def is_dsp_col(col):
  if col==9 or col==14 or col==31 or col==43 or col==48 or col==61 or col==66 or col==91 or col==96:
  	return 1
  return 0

# Returns the column number for the dsp as dsp columns dont use board cols for constraints
def get_dsp_col(col):
  x = -1
  if col == 9 :
    x = 0
  if col == 14 :
    x = 1
  if col == 31 :
    x = 2
  if col == 43 :
    x = 3
  if col == 48 :
    x = 4
  if col == 61 :
    x = 5
  if col == 66 :
    x = 6
  if col == 91 :
    x = 7
  if col == 96 :
    x = 8

  return x

def main():
  #Check inputs are correct
  correct_inputs = False
  constraint_file_specified = False
  constraint_file_name = ""
  results_file_specified = False
  results_file = ""
  research_mb = False
  help = False

  i=1

  #While loop to handle the runtime options
  while i < len(sys.argv):
    if sys.argv[i] == "-h":
      # Help file
      help = True
      i = i+1
    elif sys.argv[i] == "-c":
      # Specifying the constraint file  
      # Check that the next arg is present
      if i+1 < len(sys.argv):
        if os.path.isfile(sys.argv[i+1]):
          #Then  valid constraint file was present
          constraint_file_name = sys.argv[i+1]
          constraint_file_specified = True
          i = i+1
      i=i+1
    elif sys.argv[i] == "-i":
      # Specifying the input file
      # Check that the constraint file is correct
      if i+1 < len(sys.argv):
        if os.path.isfile(sys.argv[i+1]):
          #Then  valid constraint file was present
          results_file = sys.argv[i+1]
          results_file_specified = True
          i = i+1
      i=i+1
    elif sys.argv[i] == "-r":
      # Then the testing microblaze testing region.  
      i = i+1
      research_mb = True  
    else:
      i =i+1

  if constraint_file_specified and results_file_specified:
    # Should only procedd with xdc gen if both the constraint file and the results file were specified
    correct_inputs = True

  if help:
    print_help_info()
  if not correct_inputs:
    # Give helpful infot so that they can be specified correctly
    print("Need to specify the correct number of arguements.")
    if not help:
      print("Run 'python3 xdc.py -h' for help.") 
  else:
    # Correct inputs received, can go ahead and generate the xdc file
    print("Generating xdc file ...\n")
    constraints = extract_standard_constraints(constraint_file_name)
    # Create a constraint file
    xdc_file = open(constraint_file_name,'w+')

    # Extract the information required to create pblocks for the xdc file
    regions = extract_regions(results_file)
    for data in regions:
      create_pblock(data[0],xdc_file)
      x1 = data[1]
      x2 = data[2] 
      y1 = data[3]
      y2 = data[4]

      if(data[0] == "fip_inst" and research_mb):
        # For fault injection testing the microblaze region needed to be specified here
        s = "resize_pblock [get_pblocks fip_inst] -add {SLICE_X0Y111:SLICE_X47Y144}\n"
        xdc_file.write(s)
        s = "resize_pblock [get_pblocks fip_inst] -add {DSP48_X0Y46:DSP48_X2Y57}\n"
        xdc_file.write(s)
        s = "resize_pblock [get_pblocks fip_inst] -add {RAMB18_X0Y46:RAMB18_X2Y57}\n"
        xdc_file.write(s)
        s = "resize_pblock [get_pblocks fip_inst] -add {RAMB36_X0Y23:RAMB36_X2Y28}\n"
        xdc_file.write(s)
        s = "set_property CONTAIN_ROUTING 1 [get_pblocks " + "fip_inst" + "]\n"
        xdc_file.write(s)
        s = "set_property EXCLUDE_PLACEMENT 1 [get_pblocks " + "fip_inst" + "]\n"
        xdc_file.write(s)

      else:
        write_slice_constraints(data[0],x1,x2,y1,y2,xdc_file)
        write_dsp_constraints(data[0],x1,x2,y1,y2,xdc_file)
        write_bram_constraints(data[0],x1,x2,y1,y2,xdc_file)
        write_other_pblock_constraints(data[0],xdc_file,research_mb)


      # Writing to XDC file completed for that pblock
      xdc_file.write("\n")

    #This creates some constraints that are related to IO (based off mmult for now)
    write_standard_constraints(xdc_file,constraints)

    xdc_file.close()

if __name__ == '__main__':
  main()
