create_pblock top_inst/main_inst_r0
add_cells_to_pblock [get_pblocks top_inst/main_inst_r0] [get_cells -quiet [list top_inst/main_inst_r0]]
resize_pblock [get_pblocks top_inst/main_inst_r0] -add {SLICE_X102Y0:SLICE_X103Y199}
resize_pblock [get_pblocks top_inst/main_inst_r0] -add {RAMB18_X6Y0:RAMB18_X6Y79}

create_pblock top_inst/main_inst_r1
add_cells_to_pblock [get_pblocks top_inst/main_inst_r1] [get_cells -quiet [list top_inst/main_inst_r1]]
resize_pblock [get_pblocks top_inst/main_inst_r1] -add {DSP48_X6Y0:DSP48_X6Y79}

create_pblock top_inst/main_inst_r2
add_cells_to_pblock [get_pblocks top_inst/main_inst_r2] [get_cells -quiet [list top_inst/main_inst_r2]]
resize_pblock [get_pblocks top_inst/main_inst_r2] -add {SLICE_X100Y50:SLICE_X101Y149}

create_pblock fip_inst/mb_sys_inst
add_cells_to_pblock [get_pblocks fip_inst/mb_sys_inst] [get_cells -quiet [list fip_inst/mb_sys_inst]]
resize_pblock [get_pblocks fip_inst/mb_sys_inst] -add {SLICE_X0Y100:SLICE_X77Y249}
resize_pblock [get_pblocks fip_inst/mb_sys_inst] -add {DSP48_X0Y40:DSP48_X4Y99}
resize_pblock [get_pblocks fip_inst/mb_sys_inst] -add {RAMB18_X0Y40:RAMB18_X3Y99}

set_property PACKAGE_PIN R4 [get_ports clk_in]
set_property IOSTANDARD LVCMOS33 [get_ports clk_in]
set_property PACKAGE_PIN V18 [get_ports uart_rx]
set_property IOSTANDARD LVCMOS33 [get_ports uart_rx]
set_property PACKAGE_PIN G4 [get_ports rst]
set_property IOSTANDARD LVCMOS12 [get_ports rst]
set_property PACKAGE_PIN AA19 [get_ports uart_tx]
set_property IOSTANDARD LVCM0S33 [get_ports uart_tx]
