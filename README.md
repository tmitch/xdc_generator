# README #

This program is used to create a constraint file to be used to implement designs using Vivado. 
This constraint generator is to be used with the Artix-7 200T FPGA device. 

It takes in the solution found by the feasible placements solver and translates its result
into a format that can be used during the implementation by Vivado to place modules in
specific device regions.

### Running the program ###

To run the program, you need to specify the .dat file which contains the solution from the feaible placements solver and also the location fo the new xdc file to be created.

e.g.

Python3 xdc.py ./results.dat ../constraints.xdc